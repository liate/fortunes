This is just a few fortune files I've made and added to my /usr/share/fortune 
directory over time.  Files include:

### jargon-cookie

[The Jargon File](http://www.catb.org/jargon/), just with the more lore-oriented
parts removed.

Example:

```
% fortune jargon-cookie 
leech

1. n. (Also leecher.) Among BBS types, crackers and warez d00dz, one
who consumes knowledge without generating new software, cracks, or
techniques. BBS culture specifically defines a leech as someone who
downloads files with few or no uploads in return, and who does not
contribute to the message section. Cracker culture extends this
definition to someone (a lamer, usually) who constantly presses
informed sources for information and/or assistance, but has nothing to
contribute. See troughie.
 
2. v. [common, Toronto area] v. To download a file across any kind of
internet link. "Hop on IRC later so I can leech some MP3s from you."
Used to describe activities ranging from FTP, to IRC DCC-send, to ICQ
file requests, to Napster searches (but never to downloading email with
file attachments; the implication is that the download is the result of
a browse or search of some sort of file server). Seems to be a holdover
from the early 1990s when Toronto had a very active BBS and warez
scene. Synonymous with snarf (sense 2), and contrast snarf (sense 4)
```

### quotes

Just random quotes I've found and thought worth adding to fortune files.
These are mostly attributed to some extent.

### welch-rpg

[2475 things Mr. Welch can no longer do during an RPG](http://theglen.livejournal.com/16735.html) 
turned into a fortune file.  I think we can all agree this is a worthy 
thing to be turned into a fortune file.

Example:

```
% fortune welch-rpg
1230.  It’s okay if you name your Kindred Alucard.  But I still can’t
name my Garou Namflow.
```


### lang-hist-wrong

[A Brief, Incomplete, and Mostly Wrong History of Programming Languages](http://james-iry.blogspot.com/2009/05/brief-incomplete-and-mostly-wrong.html) 
turned into a fortune file.

Example:

```
% fortune lang-hist-wrong
1987 - Larry Wall falls asleep and hits Larry Wall's forehead on 
the keyboard. Upon waking Larry Wall decides that the string of 
characters on Larry Wall's monitor isn't random but an example 
program in a programming language that God wants His prophet, Larry 
Wall, to design. Perl is born.
    --from http://james-iry.blogspot.com/2009/05/brief-incomplete-and-mostly-wrong.html
```


### overlord-fortunes

[The Overlord List](http://www.eviloverlord.com/lists/overlord.html)
and it's accompianing documents [cellblock a](http://www.eviloverlord.com/lists/dungeon_a.html)
[and b](http://www.eviloverlord.com/lists/dungeon_b.html).  Classic trope things!

Example:

```
% fortune overlord-fortunes
   12. One of my advisors will be an average five-year-old child. Any flaws
       in my plan that he is able to spot will be corrected before
       implementation.
```


### hakmem-fortune

[HAKMEM](http://inwap.com/pdp10/hbaker/hakmem/hakmem.html) (also known
as AI Memo 239) turned into a fortune file.  It's a _lot_ of random math
trivia stuff.

Example:

```
% fortune hakmem-fortune
  ITEM 171 (Gosper):

   Since integer division can never produce a larger quotient than dividend,
   doubling the dividend and divisor beforehand will distinguish division by
   zero from division by 1 or anything else, in situations where division by
   zero does nothing.

```


### df-quotes

The Dwarf Fortress wiki's [quote list](http://dwarffortresswiki.org/index.php/Main_Page/Quote/list),
turned into a fortune file.

Example:

```
% fortune df-quotes
"There was a typo in the siegers' campfire code. When the fires went out,
so did the game."
    --Toady One
```


### 37-things-shouldnt-do-in-assoc-worlds

[This](https://eldraeverse.com/2016/08/24/37-things-you-probably-shouldnt-do-but-are-allowed-to-in-the-associated-worlds/) turned into a fortune file.
Inspired by Skippy's List, but set in Alistair Young's Eldraeverse,
as worldbuilt in his [blog](https://eldraeverse.com).

Example:

```
% fortune 37-things-shouldnt-do-in-assoc-worlds
From "37 Things You Probably Shouldn’t Do (But Are Allowed To) In The Associated
Worlds" <https://ptpb.pw/Gzrq>:

- It's bad form to get better service from petty officials by announcing yourself
  as /Unchosen Heir to the Dragon Throne, Successor of the Imperium/ if you
  don't really need to. One day they'll figure out that that's /everybody/.
```


### kjprogramming
kingjamesprogramming is Markov chain trained on the KJV, SICP, and assorted
works of ESR and Lovecraft.  This is [it's entire archive](http://kingjamesprogramming.tumblr.com/)
as a fortune file.  Mostly just the quotes from it to plain text, with some
formatting work on the code.

Example

```
% fortune kjprogramming
22:15 So he came to Nazareth, where he had more time for research, worked on a
plan for establishing a ``polytechnic institute," and served as Massachusetts's
first State Inspector of Gas Meters.
    --kingjamesprogramming
```

### beos-error-haiku
BeOS, an os made (from scratch!) in the late '90s and generally well-loved.  One
of its fun quirky things was it's web browser's error messages, which were all
haiku.  Found [here](http://8325.org/haiku/).

Example

```
% fortune beos-error-haiku
Ephemeral site.
I am the Blue Screen of Death.
No one hears your screams.
```

### bofh-excuses
Excuses of the BOFH, from [here](http://zork.net/fortunes/bofh-excuses)

Example

```
% fortune bofh-excuses
BOFH excuse #437:

crop circles in the corn shell
```
